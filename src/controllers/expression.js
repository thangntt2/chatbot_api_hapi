import Boom from 'boom'
import { map } from 'bluebird'
import Expression from '../models/Expression'
import InlineEntity from '../models/InlineEntity'
import Entity from '../models/Entity'

/**
 * Append new `value` to Entity with `id` = `entityId`
 * if `limit` is set, entity's values set will be limited to length = limit
 * here, with Entity type = keyword, limit = 10, freetext is 5 and trait is unlimited
 */
async function appendEntityValue(inlineEntity) {
  const entity = await new Entity({ id: inlineEntity.get('entityId') }).fetch()
  const oldValue = JSON.parse(entity.get('value'))

  let limit = 0
  switch (entity.get('type')) {
    case 'trait':
      limit = undefined
      break
    case 'keyword':
      limit = 10
      break
    case 'freetext':
      limit = 5
      break
    default:
      break
  }
  if (limit && oldValue.length >= limit) {
    return
  }

  const newValue = oldValue ? JSON.stringify(oldValue.concat(inlineEntity.get('value')))
    : JSON.stringify([].concat(inlineEntity.get('value')))

  await entity.set('value', newValue).save()
}

export async function create(req) {
  const richExpression = req.payload
  const expression = await Expression.forge({ sentence: richExpression.sentence }).save()
  if (!expression) {
    throw Boom.notAcceptable('Cannot create data')
  }

  const inlineEntities = await map(richExpression.inlineEntities, async (inlineEntity) => {
    const inlineEntityIns = await InlineEntity.forge({
      value: expression.get('sentence').slice(inlineEntity.start, inlineEntity.end),
      ...inlineEntity,
      expressionId: expression.id,
    }).save()

    appendEntityValue(inlineEntityIns)

    return inlineEntityIns.toJSON()
  })

  const resultExpression = {
    ...expression.attributes,
    inlineEntities,
  }
  return (JSON.stringify(resultExpression))
}

export async function getAll() {
  const expressions = await Expression.fetchAll()
  if (!expressions) {
    throw Boom.notFound('Data not found')
  }

  const richExpressions = await map(expressions.toArray(), async (expression) => {
    const richExpression = await expression.fetch({
      withRelated: ['inlineEntities'],
    })
    return richExpression.toJSON()
  })
  return richExpressions
}
